#!/usr/bin/python3

import logging
import os.path
import sys
import time

from urllib.error import HTTPError
from urllib.parse import unquote, urlencode
from urllib.request import urlopen

logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)
log = logging.getLogger(__name__)

BASE_URL = 'http://pgp.mit.edu:11371/pks/lookup?{}'


def get_raw_result(op, search):
    query_str = urlencode(
        {'options': 'mr', 'op': op, 'search': search})
    out = {}

    with urlopen(BASE_URL.format(query_str)) as raw_res:
        out['code'] = raw_res.getcode()
        out['url'] = raw_res.geturl()
        out['info'] = raw_res.info()
        out['body'] = unquote(raw_res.read().decode())

    return out

with open('collected_emails.txt', 'a') as log_file:
    for email in sys.stdin:
        email = email.strip()
        try:
            res = get_raw_result('index', email)
        except HTTPError:
            log.info('Contact %s not found!', email)
        else:
            if res['code'] == 200:
                ids = [x.strip().split(':')[1] for x in res['body'].split('\n')
                       if x.strip().startswith('pub:')]
                time.sleep(3)

                log_file.write('{0}: {1}\n'.format(email, ','.join(ids)))

                for key_id in ids:
                    get_fn = '{}.asc'.format(key_id[:8])
                    if not os.path.exists(get_fn):
                        get_res = get_raw_result('get', '0x{}'.format(key_id))
                        if get_res['code'] == 200:
                            with open(get_fn, 'w') as outf:
                                outf.write(get_res['body'])
                        else:
                            log.error('Špatně, code = %s', get_res['code'])
                        time.sleep(3)
