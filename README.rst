Very primitive script downloading PGP public keys from a
keyserver (currently hardcoded is ``pgp.mit.edu``) for all email
addresses on stdin.

I try to be gentle to the server so there are ``time.sleep``
commands dispersed in the script.
